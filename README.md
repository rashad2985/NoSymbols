# NoSymbols
NoSymbols is a small tool, that converts the input string into another string (output) containing only ASCII Letters.

## Problem
Some times application (or method in it) have to write into or read from a file special characters, those are not letters or letters does not belog to the ASCII letters. Some times developer needs to escape these special characters, sometimes some characters are forgotten to be escaped. If there is a new use case or new symbol, then developer needs to edit the code again.These can lead to the problems like wrong display of the characters on the view elements, after escaping and saving them, redundance code parts, code editing/change in different parts of the source code. In such situations, most of the developers code hard. And there is already known problems for the hard coded source codes.

## Solution
There are already number of solutions for the problem described above. But still, some times, there are solutions, those are more efficient and smart enough, naturally loosing some features or specifications.
The idea is, to write an universal converter for characters, that uses always a character set of the safe characters, e.g. only letters (lower or upper case) from ASCII, and maps the input characters to the characters from safe set.

## How it works
The idea is simple, split charter, that consists from 8-32 bits, to 4-bit parts (0000..1111 <=> 0..15) and add it onto the ASCII character 'A' for upper case output (or 'a' for lower case output). You can subtract this from 'Z' (or 'z' for lower case output) or add to the different letter, e.g. 'B' (but chosen letter must not overflow the safe character set after adding 15).

*Example: character '&': ASCII code is 38 in decimal and 00100110 in binary. After splitting it to the 2 4-bit parts, there are left part - 0010 (2 in decimal) and right part - 0110 (6 in decimal). Use 'A' as a base, they turn into 'A'+2='C' and 'A'+6='G'. Just concatenate them and "CG" is the representation of the '&'.*

On reading just take 2 symbols at a time to build one. (Or 4 for longer encodings that consist from 16-bits etc.).
This converter just does the job. Using it as a write/read layer between app and file, developer can avoid the most errors like escape or errors on save and read hashed passwords etc.

## Drawbacks
As I early described, there are naturally some drawbacks, if this tool is used:

1. Space used after convertion is at least doubles itself: 2x for 8-bit, 4x for 16-bit and 8x for 32-bit character sets will be used.
2. The raw output is not easily readable for humans.
3. Convertion time to the both directions is linear to the input length, but with a constant factor of k=(bits per input character) / 4. This one is a littlebit tricky. If the constant factor is smaller than the number of escaped characters, then this tool converts faster than escaping. Else escaping spacial characters would be faster than this tool. 
