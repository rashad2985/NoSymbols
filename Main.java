public class Main {

	/**
	* This main class is only for testing here. You can use it to play with the tool.
	* Just call the compiled Main.class with string parameter.
	* e.g.: After compilation: java Main "Your string as a parapeter to convert"
	*/
	public static void main(String[] args) {
		String str = "Test";
		
		if (args != null && args.length > 0) {
			if (!args[0].isEmpty()) {
				str = args[0];
			}
		}
		System.out.println("Input:\t\t" + str);
		
		String enc = encode(str);
		System.out.println("Encoded:\t" + enc);
		
		String dec = decode(enc);
		System.out.println("Decoded:\t" + dec);
		
	}

	/**
	* Takes a string as a parameter and encodes it to the symbols between 'A' and 'O'.
	* Encoded string takes more place, because of the conversion each 4-bit to the 8-bit ANSII. 
	*/
	public static String encode(String str) {
		byte[] strAsBytes = str.getBytes(); // convert string to the bytes
		byte[] bytes = new byte[strAsBytes.length * 2]; // converted bytes will be in this
		
		// coverting for each character in the str
		for (int i = 0; i < strAsBytes.length; i++) {
			bytes[2 * i] = (byte) (((strAsBytes[i] & 0x00F0) >> 4) + 'A'); // first 4 bits of the byte
			bytes[2 * i + 1] = (byte) ((strAsBytes[i] & 0x000F) + 'A'); // last 4 bits of the byte
		}
		
		// returning the converted string as a new string, that contains only letter 'A'..'O'.
		return (new String(bytes));
	}

	/**
	* Takes a string as a parameter and decodes it to the symbols of the original String. 
	*/
	private static String decode(String str) {
		if (str == null || str.isEmpty()) {
			return ""; // there is nothing to convert
		}
		
		// byte consists of the 8 bits => even count of the charaters needed
		if (str.length() % 2 != 0) {
			return "Invalid count of characters!";
		}
		
		char[] strAsChars = str.toCharArray(); // characters to be combined
		byte[] bytes = new byte[strAsChars.length / 2]; // this will contain original characters
		
		// converting
		for (int i = 0; i < bytes.length; i++) {
			// char, because if the MSB of the byte is 1 => it will be considered as a negative number.
			char left = (char) ((strAsChars[2 * i] - 'A') << 4); // extracting left 4 bits
			char right = (char) ((strAsChars[2 * i + 1] - 'A') & 0x0F); // extracting right 4 bits
			bytes[i] = (byte) (left + right); // combining them into the one byte
		}
		// it is ready to return
		return (new String(bytes));
	}	
}
